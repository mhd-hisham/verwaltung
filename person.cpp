#include "person.h"

Person::Person(){
}
Person::~Person(){}
Person::Person(int Person_id){
    this->Person_id = Person_id;
}
Person* Person::set_vorname(QString name){
    this->vorname = name;
    return  this;
}
Person* Person::set_nachname(QString name){
    this->nachname = name;
    return  this;
}

Person* Person::set_Person_id(int nr){
    this->Person_id = nr;
    return this;
}


QString Person::get_vorname(){
    return this->vorname;
}
QString Person::get_nachname(){
    return this->nachname;
}

int Person::get_Person_id(){
    return this->Person_id;
}

Person& Person::operator=(const Person& m){
    this->set_vorname(m.vorname);
    this->set_nachname(m.nachname);
    this->set_Person_id(m.Person_id);
    return *this;
}

void Person::clear(){
    this->set_vorname("");
    this->set_nachname("");
    this->set_Person_id(0);
}

bool Person::is_valid(){
    return this->get_vorname() != "" &&
            this->get_nachname() != "" ;
}
