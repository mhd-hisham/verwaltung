#ifndef ADRESS_H
#define ADRESS_H

#include "header.h"
class Adress{
public:
    Adress(QString ort="", QString str="", QString PLZ="", QString hausNr=""){
        this->ort = ort;
        this->strasse = str;
        this->PLZ = PLZ;
        this->hausNr = hausNr;
    }

    Adress* set_Ort(QString ort=""){
        this->ort = ort;
        return this;
    }

    Adress* set_Strasse(QString str=""){
        this->strasse = str;
        return this;
    }

    Adress* set_PLZ(QString plz=""){
        this->PLZ = plz;
        return this;
    }

    Adress* set_hausNr(QString nr=""){
        this->hausNr = nr;
        return this;
    }

    QString get_ort(){
        return this->ort;
    }
    QString get_PLZ(){
        return this->PLZ;
    }
    QString get_strasse(){
        return this->strasse;
    }
    QString get_hausNr(){
        return this->hausNr;
    }

private:
    QString ort;
    QString strasse;
    QString PLZ;
    QString hausNr;
};



#endif // ADRESS_H
