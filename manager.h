#ifndef MANAGER_H
#define MANAGER_H
#include "mitgliedmanager.h"
#include "zahlungmanager.h"

class Manager
{
public:
    Manager(MitgliedManager &mitglied, ZahlungManager &zahlung);
private:
    MitgliedManager *mitglied;
    ZahlungManager *zahlung;
};

#endif // MANAGER_H
