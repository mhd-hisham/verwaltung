#ifndef VERWALTUNG_H
#define VERWALTUNG_H

//#include <QMainWindow>
#include "mitgliedmanager.h"
#include "zahlungmanager.h"
#include "person.h"
#include "loggingcategories.h"

#include "header.h"


namespace Ui {
class Verwaltung;
}

class Verwaltung : public QMainWindow
{
    Q_OBJECT

public:
    explicit Verwaltung(QWidget *parent = nullptr);
    ~Verwaltung();

private slots:
    void on_actionZahlen_triggered();

    void on_zahlen_ID_currentIndexChanged(int index);

    void on_zahlen_name_currentIndexChanged(int index);

    void on_actionMitglied_Table_triggered();

    void on_actionZahlung_Table_triggered();

    void on_zahlen_buttonBox_rejected();

    void on_actionhinzuf_gen_triggered();

    void on_insert_acctept_accepted();

    void on_Vorname_textChanged(const QString &arg1);

    void on_Nachname_textChanged(const QString &arg1);

    void on_GebutsDatum_dateChanged(const QDate &date);

    void on_telefon_nr_textChanged(const QString &arg1);

    void on_PLZ_textChanged(const QString &arg1);

    void on_ORT_textChanged(const QString &arg1);

    void on_Strasse_textChanged(const QString &arg1);

    void on_HausNr_textChanged(const QString &arg1);

    void on_Monatlische_Betrag_textChanged(const QString &arg1);

    void on_insert_acctept_rejected();

    void on_zahlen_buttonBox_accepted();

    void on_actionDruck_triggered();


    void on_actionBeschreibung_triggered();

    void on_Mitglied_table_clicked(const QModelIndex &index);

    void on_ID_bearbeiten_currentIndexChanged(int index);

    void on_insert_acctept_2_accepted();

    void on_Vorname_2_textChanged(const QString &arg1);

    void on_Nachname_2_textChanged(const QString &arg1);

    void on_GebutsDatum_2_dateChanged(const QDate &date);

    void on_telefon_nr_2_textChanged(const QString &arg1);

    void on_PLZ_2_textChanged(const QString &arg1);

    void on_ORT_2_textChanged(const QString &arg1);

    void on_Strasse_2_textChanged(const QString &arg1);

    void on_HausNr_2_textChanged(const QString &arg1);

    void on_Monatlische_Betrag_2_textChanged(const QString &arg1);

    void on_actionbearbeiten_triggered();

    void on_actionhinzuf_gen_2_triggered();

    void on_actionTable_triggered();

    void on_actionzahlen_triggered();

    void on_actionTabelle_triggered();

    void on_abgeben_button_clicked();

    void on_actionKasse_triggered();

    void on_mitglied_table_mehr_clicked();

    void on_zahlung_table_mehr_clicked();

    void on_kasse_table_mehr_clicked();

    void on_suche_bottom_accepted();

    void on_suche_comboBox_currentIndexChanged(const QString &arg1);

    void on_suche_vorname_textChanged(const QString &arg1);

    void on_suche_Nachname_textChanged(const QString &arg1);

    void on_suche_bottom_rejected();

    void on_suchen_table_clicked(const QModelIndex &index);

    void on_actionsuche_mitglied_triggered();

    void on_Suche_bottom_clicked();

    void on_area_zu_clicked();

    void on_actionNoch_nicht_bezahlt_triggered();

private:
    void lineEditEinstellen();
    void tool_init();
    void zahlen_init();
    void mitglied_init();
    void mitglied_table_fill();
    void zahlung_table_fill();
    void kasse_table_fill();
    void combobox_fill();
    void kasse_init();
    void kasse_update();
    void set_beschreibung(Mitglied m);
    void set_bearbeiten(Mitglied m);
    Ui::Verwaltung *ui;
    MitgliedManager mitgliedManager;
    ZahlungManager zahlungManager;
    std::unique_ptr<QGridLayout> tool_layout = std::unique_ptr<QGridLayout>(new QGridLayout);
    std::unique_ptr<QWidget> tool_widget = std::unique_ptr<QWidget>(new QWidget);
    std::unique_ptr<QLabel> kasse_TXT = std::unique_ptr<QLabel>(new QLabel("Kasse : "));
    std::unique_ptr<QLabel> kasse = std::unique_ptr<QLabel>(new QLabel(""));
    Mitglied m_f_z; //mitglied fuer zahlen
    Mitglied m_bearbeiten; //mitglied fuer bearbeiten
    Person person; // fuer suchen
};

#endif // VERWALTUNG_H
