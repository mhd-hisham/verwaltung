#ifndef MITGLIED_H
#define MITGLIED_H

//#include <iostream>
//#include <QDate>
#include "adress.h"
#include "header.h"
using namespace std;
class Mitglied
{
public:
    Mitglied();
    Mitglied(int mitglied_id);
    Mitglied* set_vorname(QString name);
    Mitglied* set_nachname(QString name);
    Mitglied* set_alter(int alter);
    Mitglied* set_mitglied_id(int nr);
    Mitglied* set_geburts_datum(QDate datum);
    Mitglied* set_mitglied_datum(QDate datum);
    Mitglied* set_adress(Adress adress);
    Adress* set_ort(QString ort);
    Adress* set_plz(QString plz);
    Adress* set_strasse(QString str);
    Adress* set_haus_nr(QString haus);
    Mitglied* set_betrag(double betrag);
    Mitglied* set_telefon(QString telefon);
    Mitglied* set_last_payment(QDate date);
    QDate get_last_payment();
    QString get_vorname();
    QString get_nachname();
    int get_alter();
    int get_mitglied_id();
    QDate get_geburts_datum();
    QDate get_mitglied_datum();
    Adress get_adress();
    double get_betrag();
    QString get_telefon();
    void clear();
    ~Mitglied();
    Mitglied& operator=(const Mitglied& m);
    bool is_valid();
private:
    QString vorname;
    QString nachname;
    int alter;
    int mitglied_id;
    QDate geburts_datum;
    QDate mitglied_datum;
    QDate last_payment;
    Adress adress;
    double betrag = 0.0;
    QString telefon;
};

#endif // MITGLIED_H
