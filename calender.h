#ifndef CALENDER_H
#define CALENDER_H

#include <QCalendarWidget>
#include <QListWidget>
#include <iostream>
#include <memory>
#include <QDateEdit>
#include <QTextCharFormat>
#include <QList>
#include <QHash>
#include <QListWidgetItem>
class Calender
{
private:
    Calender();
    std::unique_ptr<QCalendarWidget> myCalendar = std::unique_ptr<QCalendarWidget>(new QCalendarWidget);
    std::unique_ptr<QListWidget> mylist = std::unique_ptr<QListWidget>(new QListWidget);
public:
    static Calender& instance();
    void set_calender(QCalendarWidget * cal, QListWidget * list);
    void select_date(QDate date, Qt::GlobalColor color, QString info );
    QHash<QDate,QList<QString>> hash;

};

#endif // CALENDER_H
