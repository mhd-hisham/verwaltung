#-------------------------------------------------
#
# Project created by QtCreator 2019-06-06T23:26:34
#
#-------------------------------------------------

QT    += core gui printsupport sql
QT += network
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Verwaltung
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11
INCLUDEPATH += C:\boost_1_70_0
LIBS += -lws2_32

RESOURCES += Verwaltung.qrc

SOURCES += \
        Kasse.cpp \
        loggingcategories.cpp \
        main.cpp \
        mitglied.cpp \
        mitgliedmanager.cpp \
        person.cpp \
        server.cpp \
        verwaltung.cpp \
        zahlung.cpp \
        zahlungmanager.cpp

HEADERS += \
        Kasse.h \
        adress.h \
        header.h \
        loggingcategories.h \
        mitglied.h \
        mitgliedmanager.h \
        person.h \
        server.h \
        verwaltung.h \
        zahlung.h \
        zahlungmanager.h

FORMS += \
        verwaltung.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
