#include "zahlungmanager.h"

void ZahlungManager::init(){

}
ZahlungManager::ZahlungManager()
{
}


bool ZahlungManager::fill_zahlung_table(){
    list<Zahlung> list = server->get_Zahlungen();
    if(list.empty()){
        return false;
    }
    for_each(list.begin(),list.end(),[&](Zahlung z){
       add_zahlung(z,true);
    });
    return true;
}

bool ZahlungManager::fill_kasse_table(){
    list<Kasse> list = server->get_Kassen();
    if(list.empty()){
        return false;
    }
    for_each(list.begin(),list.end(),[&](Kasse k){
        add_kasse(k,true);
    });
    return true;
}


bool ZahlungManager::add_zahlung(Zahlung zahlung,bool from_server){
    int id = -1;
    if(!from_server){
            id = server->set_zahlung(zahlung);
            if(id < 1){ //Fehler auftreten
                return false;
            }
            zahlung.set_zahlung_nr(id);
    }

    int row = zahlung_table->model()->rowCount();

    model->setItem(row, 0, new QStandardItem(QString::number(zahlung.get_zahlung_nr())));
    model->setItem(row, 1, new QStandardItem(QString::number(zahlung.get_mitglied_nr())));
    model->setItem(row, 2, new QStandardItem(QString::number(zahlung.get_betrag())));
    model->setItem(row, 3, new QStandardItem(zahlung.get_datum().toString()));
    return true;
}

bool ZahlungManager::add_kasse(Kasse kasse,bool from_server){
    int id = -1;
    if(!from_server){
            id = server->set_kasse(kasse);
            if(id < 1){ //Fehler auftreten
                return false;
            }
            kasse.set_kasse_nr(id);
    }

    int row = kasse_table->model()->rowCount();

    kasse_model->setItem(row, 0, new QStandardItem(QString::number(kasse.get_kasse_nr())));
    kasse_model->setItem(row, 1, new QStandardItem(QString::number(kasse.get_betrag())));
    kasse_model->setItem(row, 2, new QStandardItem(kasse.get_datum().toString()));
    return true;
}

void ZahlungManager::get_Zahlung(int mitglied_nr){
    suche_list = server->get_zahlung(mitglied_nr);
    for_each(suche_list.begin(),suche_list.end(),[&](Zahlung zahlung){
        int row = suchen_table->model()->rowCount();
        suche_model->setItem(row, 0, new QStandardItem(QString::number(zahlung.get_zahlung_nr())));
        suche_model->setItem(row, 1, new QStandardItem(QString::number(zahlung.get_mitglied_nr())));
        suche_model->setItem(row, 2, new QStandardItem(QString::number(zahlung.get_betrag())));
        suche_model->setItem(row, 3, new QStandardItem(zahlung.get_datum().toString()));
    });
}


void ZahlungManager::update_table(Zahlung z){
    qDebug() << "update table\n";
        for(int i = 0; i < zahlung_table->model()->rowCount(); i++){
            if(zahlung_table->model()->data(zahlung_table->model()->index(i,0)).toInt() == z.get_zahlung_nr()){
                zahlung_table->model()->setData(zahlung_table->model()->index(i,1),QString::number(z.get_zahlung_nr()));
                zahlung_table->model()->setData(zahlung_table->model()->index(i,2),QString::number(z.get_betrag()));
                zahlung_table->model()->setData(zahlung_table->model()->index(i,3),z.get_datum().toString());
                zahlung_table->model()->submit();
            }
        }
}



void ZahlungManager::set_Table(QTableView *zahlung_table, QTableView *kasse_table){
    this->zahlung_table.reset(zahlung_table);
    this->kasse_table.reset(kasse_table);
    Table_Init();
}

void ZahlungManager::set_suche_Table(QTableView *table){
    suchen_table.reset(table);
    suche_Table_Init();
}

void ZahlungManager::Table_Init(){
    model->setHeaderData(0,Qt::Horizontal,QObject::tr("ID"));
    model->setHeaderData(1,Qt::Horizontal,QObject::tr("Mitglied_ID"));
    model->setHeaderData(2,Qt::Horizontal,QObject::tr("Betrag"));
    model->setHeaderData(3,Qt::Horizontal,QObject::tr("Datum"));

    kasse_model->setHeaderData(0,Qt::Horizontal,QObject::tr("ID"));
    kasse_model->setHeaderData(1,Qt::Horizontal,QObject::tr("Betrag"));
    kasse_model->setHeaderData(2,Qt::Horizontal,QObject::tr("Datum"));

    zahlung_table->setModel(model);
    zahlung_table->setSortingEnabled(true);
    zahlung_table->sortByColumn(0, Qt::AscendingOrder);

    kasse_table->setModel(kasse_model);
    kasse_table->setSortingEnabled(true);
    kasse_table->sortByColumn(0, Qt::AscendingOrder);

}

void ZahlungManager::suche_Table_Init(){
    suche_model->setHeaderData(0,Qt::Horizontal,QObject::tr("ID"));
    suche_model->setHeaderData(1,Qt::Horizontal,QObject::tr("Mitglied_ID"));
    suche_model->setHeaderData(2,Qt::Horizontal,QObject::tr("Betrag"));
    suche_model->setHeaderData(3,Qt::Horizontal,QObject::tr("Datum"));

    suchen_table->setModel(suche_model);
    suchen_table->setSortingEnabled(true);
    suchen_table->sortByColumn(0, Qt::AscendingOrder);
}





double ZahlungManager::get_kasse(){
    return server->get_kasse();
}

void ZahlungManager::print_table(){
    QString strStream;
    QTextStream out(&strStream);

    const int rowCount = zahlung_table->model()->rowCount();
    const int columnCount = zahlung_table->model()->columnCount();

    out <<  "<html>\n"
        "<head>\n"
        "<meta Content=\"Text/html; charset=Windows-1251\">\n"
        <<  QString("<title>%1</title>\n").arg("strTitle")
        <<  "</head>\n"
        "<body bgcolor=#ffffff link=#5000A0>\n"
        "<table border=1 cellspacing=0 cellpadding=2>\n";

    // headers
    out << "<thead><tr bgcolor=#f0f0f0>";
    for (int column = 0; column < columnCount; column++)
        if (!zahlung_table->isColumnHidden(column))
            out << QString("<th>%1</th>").arg(zahlung_table->model()->headerData(column, Qt::Horizontal).toString());
    out << "</tr></thead>\n";

    // data table
    for (int row = 0; row < rowCount; row++) {
        out << "<tr>";
        for (int column = 0; column < columnCount; column++) {
            if (!zahlung_table->isColumnHidden(column)) {
                QString data = zahlung_table->model()->data(zahlung_table->model()->index(row, column)).toString().simplified();
                out << QString("<td bkcolor=0>%1</td>").arg((!data.isEmpty()) ? data : QString("&nbsp;"));
            }
        }
        out << "</tr>\n";
    }
    out <<  "</table>\n"
        "</body>\n"
        "</html>\n";

    QTextDocument *document = new QTextDocument();
    document->setHtml(strStream);

    QPrinter printer;

    QPrintDialog *dialog = new QPrintDialog(&printer, NULL);
    if (dialog->exec() == QDialog::Accepted) {
        document->print(&printer);
    }

    delete document;
}


void ZahlungManager::suche_table_leeren(){
    while (suchen_table->model()->rowCount() > 0) {
        suchen_table->model()->removeRow(0);
    }
}

