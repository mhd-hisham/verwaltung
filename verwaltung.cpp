#include "verwaltung.h"
#include "ui_verwaltung.h"

Verwaltung::Verwaltung(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Verwaltung)
{
    ui->setupUi(this);
    setWindowIcon(QIcon(":ver.png"));
    ui->actionZahlen->setIcon(QIcon(":zahlen.png"));
    ui->actionzahlen->setIcon(QIcon(":zahlen.png"));
    ui->actionhinzuf_gen->setIcon(QIcon(":plus.png"));
    ui->actionhinzuf_gen_2->setIcon(QIcon(":plus.png"));
    ui->actionDruck->setIcon(QIcon(":drucken.png"));
    ui->actionTable->setIcon(QIcon(":tabelle.png"));
    ui->actionTabelle->setIcon(QIcon(":tabelle.png"));
    ui->actionsuche_mitglied->setIcon(QIcon(":suche.png"));


    lineEditEinstellen();
    zahlen_init();
    mitglied_init();
    tool_init();

    mitglied_table_fill();
    combobox_fill();
    zahlung_table_fill();
    kasse_table_fill();
    kasse_update();
    ui->Beschreibung->hide();
    ui->ober_stacked->hide();
    ui->suche_zahlung_area->hide();

    qDebug(logDebug()) << "Verwaltung started";
}

Verwaltung::~Verwaltung()
{
    delete ui;
}


void Verwaltung::on_actionZahlen_triggered()
{
    if(ui->ober_stacked->isHidden()){
        ui->ober_stacked->show();
    }else{
        ui->ober_stacked->hide();
    }
}

void Verwaltung::lineEditEinstellen(){
    auto dv = new QDoubleValidator();
    dv->setNotation(QDoubleValidator::StandardNotation);
    ui->Monatlische_Betrag->setValidator(dv);
    ui->Monatlische_Betrag_2->setValidator(dv);
    ui->zahlen_betrag->setValidator(dv);
}


void Verwaltung::kasse_init(){
    qDebug(logDebug()) << "kasse_init";
}


void Verwaltung::tool_init(){
    qDebug(logDebug()) << "tool_init";
    tool_layout->addWidget(kasse_TXT.get(),0,0);
    tool_layout->addWidget(kasse.get(),0,1);
    tool_widget->setLayout(tool_layout.get());
    //ui->toolBar->addWidget(tool_widget.get());
    ui->mainToolBar->addWidget(tool_widget.get());
}

void Verwaltung::zahlen_init(){
    qDebug(logDebug()) << "zahlen_init";
    zahlungManager.set_Table(ui->Zahlung_table, ui->Kasse_table);
    zahlungManager.set_suche_Table(ui->suchen_table_2);
}
void Verwaltung::mitglied_init(){
    qDebug(logDebug()) << "mitglied_init";
    mitgliedManager.set_Table(ui->Mitglied_table);
    mitgliedManager.set_suche_Table(ui->suchen_table);
    mitgliedManager.set_comboBox(ui->zahlen_ID,MitgliedManager::ComboBox_Typ::ID);
    mitgliedManager.set_comboBox(ui->ID_bearbeiten,MitgliedManager::ComboBox_Typ::ID);
    mitgliedManager.set_comboBox(ui->suche_comboBox,MitgliedManager::ComboBox_Typ::ID);
    mitgliedManager.set_comboBox(ui->zahlen_name,MitgliedManager::ComboBox_Typ::Nachname);
}

void Verwaltung::mitglied_table_fill(){
    mitgliedManager.fill_table();
}

void Verwaltung::combobox_fill(){
    mitgliedManager.fill_comboboxs();
}

void Verwaltung::zahlung_table_fill(){
    zahlungManager.fill_zahlung_table();
}
void Verwaltung::kasse_table_fill(){
    zahlungManager.fill_kasse_table();
}


void Verwaltung::on_zahlen_ID_currentIndexChanged(int index)
{
    ui->zahlen_name->setCurrentIndex(index);
    int id = ui->zahlen_ID->currentText().toInt();
    Mitglied m = mitgliedManager.get_mitglied(id);
    ui->zahlen_betrag->setText(QString::number(m.get_betrag()));
    set_beschreibung(m);
}

void Verwaltung::on_zahlen_name_currentIndexChanged(int index)
{
    ui->zahlen_ID->setCurrentIndex(index);
}

void Verwaltung::on_actionMitglied_Table_triggered()
{
    ui->unter_stacked->setCurrentIndex(0);
}

void Verwaltung::on_actionZahlung_Table_triggered()
{
    ui->unter_stacked->setCurrentIndex(2);
}

void Verwaltung::on_zahlen_buttonBox_rejected()
{
    ui->ober_stacked->hide();
}

void Verwaltung::on_actionhinzuf_gen_triggered()
{
    ui->unter_stacked->setCurrentIndex(3);
    ui->GebutsDatum->setDate(QDate(2000,01,01));
}

void Verwaltung::on_insert_acctept_accepted()
{
    QMessageBox msgBox;
    m_f_z.set_mitglied_datum(QDate::currentDate());
    if(m_f_z.is_valid()){
        bool res = mitgliedManager.add_mitglied(m_f_z);
        if(res){
            m_f_z.clear();
            on_insert_acctept_rejected();
            msgBox.setText("Mitglied wird erfolgreich hinzugefuegt");
        }else{
            msgBox.setText("Fehler aufgetreten...");
        }
    }else{
        qDebug(logWarning()) << "Nicht alle Eingaben ausgefuellt bei insert Mitglied";
        msgBox.setText("Nicht alle Eingaben ausgefuellt bei insert Mitglied");
    }
    msgBox.exec();
}

void Verwaltung::on_insert_acctept_rejected()
{
    ui->Vorname->clear();
    ui->Nachname->clear();
    ui->Monatlische_Betrag->clear();
    ui->ORT->clear();
    ui->PLZ->clear();
    ui->telefon_nr->clear();
    ui->GebutsDatum->clear();
    ui->Strasse->clear();
    ui->HausNr->clear();
    ui->tolefon_lable->clear();
    m_f_z.clear();
}


void Verwaltung::on_Vorname_textChanged(const QString &arg1)
{
    m_f_z.set_vorname(arg1);
}

void Verwaltung::on_Nachname_textChanged(const QString &arg1)
{
    m_f_z.set_nachname(arg1);
}

void Verwaltung::on_GebutsDatum_dateChanged(const QDate &date)
{
    m_f_z.set_geburts_datum(date);
}

void Verwaltung::on_telefon_nr_textChanged(const QString &arg1)
{
    m_f_z.set_telefon(arg1);
}

void Verwaltung::on_PLZ_textChanged(const QString &arg1)
{
    m_f_z.set_plz(arg1);
}

void Verwaltung::on_ORT_textChanged(const QString &arg1)
{
    m_f_z.set_ort(arg1);
}

void Verwaltung::on_Strasse_textChanged(const QString &arg1)
{
    m_f_z.set_strasse(arg1);
}

void Verwaltung::on_HausNr_textChanged(const QString &arg1)
{
    m_f_z.set_haus_nr(arg1);
}

void Verwaltung::on_Monatlische_Betrag_textChanged(const QString &arg1)
{
    m_f_z.set_betrag(arg1.toDouble());
}



/*void Verwaltung::on_enter_botton_clicked()
{
    QString text = ui->suche_lineEdit->text();
    bool found = false, tmp = false;
    for(int row = 0 ; row < ui->Mitglied_table->model()->rowCount(); row++){
        for(int column = 0 ;column < ui->Mitglied_table->model()->columnCount(); column++){
            tmp = ui->Mitglied_table->model()->index(row,column).data().toString().contains(text);
            found = found|tmp;
        }
        if(!found){
            ui->Mitglied_table->hideRow(row);
        }
        found = tmp = false;
    }
}*/

/*void Verwaltung::on_clear_botton_clicked()
{
    for(int i = 0 ;i < ui->Mitglied_table->model()->rowCount(); i++)
        ui->Mitglied_table->showRow(i);
}*/



void Verwaltung::on_zahlen_buttonBox_accepted()
{
    QMessageBox msgBox;
    Zahlung z;
    double betrag = ui->zahlen_betrag->text().toDouble();
    if (betrag > 0){
        z.set_mitglied_nr(ui->zahlen_ID->currentText().toInt());
        z.set_betrag(betrag);
        z.set_datum(QDate::currentDate());
        int id = zahlungManager.add_zahlung(z);
        if(id > 0){
            Mitglied m = mitgliedManager.get_mitglied(z.get_mitglied_nr());
            m.set_last_payment(z.get_datum());
            mitgliedManager.update_table(m);
            mitgliedManager.update_list(m);
            set_bearbeiten(m);
            kasse_update();
            qDebug(logDebug()) << "Neue Zahlung "<< "ID : "<< id << "Mitglied_nr : "<<z.get_mitglied_nr();
            msgBox.setText("Neue Zahung wurde erfolgreich hinzugefuegt...");
        }else {
            qDebug(logWarning()) << "Fehler bei Zahlung aufgetreten";
            msgBox.setText("Fehler bei der Zahlung aufgetreten");
        }
    }else{
        msgBox.setText("Betrag eingeben...");
    }
    msgBox.exec();
}

void Verwaltung::kasse_update(){
    this->kasse->setText(QString::number(zahlungManager.get_kasse()));
}

void Verwaltung::on_actionDruck_triggered()
{
    if(ui->unter_stacked->currentIndex() == 0){
        mitgliedManager.print_table();
    }else if(ui->unter_stacked->currentIndex() == 2){
        zahlungManager.print_table();
    }
}

void Verwaltung::on_actionBeschreibung_triggered()
{
    if(ui->Beschreibung->isHidden()){
        ui->Beschreibung->show();
    }else{
        ui->Beschreibung->hide();
    }


}

void Verwaltung::on_Mitglied_table_clicked(const QModelIndex &index)
{
    ui->Mitglied_table->selectRow(index.row());
    QModelIndexList selected=ui->Mitglied_table->selectionModel()->selectedRows();
    QModelIndex i=selected.at(0);
    Mitglied m = mitgliedManager.get_mitglied(i.data(0).toInt());
    set_beschreibung(m);
    ui->Beschreibung->show();
}

void Verwaltung::on_suchen_table_clicked(const QModelIndex &index)
{
    ui->suchen_table->selectRow(index.row());
    QModelIndexList selected=ui->suchen_table->selectionModel()->selectedRows();
    QModelIndex i=selected.at(0);
    for_each(mitgliedManager.suche_list.begin(),mitgliedManager.suche_list.end(),[&](Mitglied m){
        if(i.data(0).toInt() == m.get_mitglied_id()){
            set_beschreibung(m);
            zahlungManager.suche_table_leeren();
            zahlungManager.get_Zahlung(m.get_mitglied_id());
            ui->suche_zahlung_area->show();
            ui->Beschreibung->show();
        }
    });
}

void Verwaltung::set_beschreibung(Mitglied m){
    ui->ID_label->setText(QString::number(m.get_mitglied_id()));
    ui->nachname_lable->setText(m.get_nachname());
    ui->vorname_lable->setText(m.get_vorname());
    ui->betrag_lable->setText(QString::number(m.get_betrag()));
    ui->gebutsdatum_lable->setText(m.get_geburts_datum().toString());
    ui->mitglied_datum_lable->setText(m.get_mitglied_datum().toString());
    ui->ort_lable->setText(m.get_adress().get_ort());
    ui->strasse_lable->setText(m.get_adress().get_strasse());
    ui->haus_lable->setText(m.get_adress().get_hausNr());
    ui->plz_lable->setText(m.get_adress().get_PLZ());
    ui->tolefon_lable->setText(m.get_telefon());
    ui->last_payment_label->setText(m.get_last_payment().toString());
}


void Verwaltung::on_ID_bearbeiten_currentIndexChanged(int index)
{
    int id = ui->ID_bearbeiten->currentText().toInt();
    Mitglied m = mitgliedManager.get_mitglied(id);
    m_bearbeiten.set_mitglied_id(id);
    m_bearbeiten.set_mitglied_datum(m.get_mitglied_datum());
    set_bearbeiten(m);
}

void Verwaltung::set_bearbeiten(Mitglied m){
    ui->Vorname_2->setText(m.get_vorname());
    ui->Nachname_2->setText(m.get_nachname());
    ui->GebutsDatum_2->setDate(m.get_geburts_datum());
    ui->telefon_nr_2->setText(m.get_telefon());
    ui->ORT_2->setText(m.get_adress().get_ort());
    ui->Strasse_2->setText(m.get_adress().get_strasse());
    ui->PLZ_2->setText(m.get_adress().get_PLZ());
    ui->HausNr_2->setText(m.get_adress().get_hausNr());
    ui->Monatlische_Betrag_2->setText(QString::number(m.get_betrag()));
}

void Verwaltung::on_insert_acctept_2_accepted()
{
    QMessageBox msgBox;
    if(m_bearbeiten.is_valid()){
        int res = mitgliedManager.update_mitglied(m_bearbeiten);
        if(res != -1){
            m_bearbeiten.clear();
            on_insert_acctept_rejected();
            msgBox.setText("Mitarbeiter wurde erfolgreich bearbeitet...");
        }else{
            msgBox.setText("Fehler aufgetreten...");
        }
    }else{
        qDebug(logWarning()) << "Nicht alle Eingaben ausgefuellt bei bearbeiten von dem Mitglied";
        msgBox.setText("Nicht alle Eingaben ausgefuellt bei bearbeiten von dem Mitglied");
    }
    msgBox.exec();
}

void Verwaltung::on_Vorname_2_textChanged(const QString &arg1)
{
    m_bearbeiten.set_vorname(arg1);
}

void Verwaltung::on_Nachname_2_textChanged(const QString &arg1)
{
    m_bearbeiten.set_nachname(arg1);
}

void Verwaltung::on_GebutsDatum_2_dateChanged(const QDate &date)
{
    m_bearbeiten.set_geburts_datum(date);
}

void Verwaltung::on_telefon_nr_2_textChanged(const QString &arg1)
{
    m_bearbeiten.set_telefon(arg1);
}

void Verwaltung::on_PLZ_2_textChanged(const QString &arg1)
{
   m_bearbeiten.set_plz(arg1);
}

void Verwaltung::on_ORT_2_textChanged(const QString &arg1)
{
    m_bearbeiten.set_ort(arg1);
}

void Verwaltung::on_Strasse_2_textChanged(const QString &arg1)
{
    m_bearbeiten.set_strasse(arg1);
}

void Verwaltung::on_HausNr_2_textChanged(const QString &arg1)
{
    m_bearbeiten.set_haus_nr(arg1);
}

void Verwaltung::on_Monatlische_Betrag_2_textChanged(const QString &arg1)
{
    m_bearbeiten.set_betrag(arg1.toDouble());
}

void Verwaltung::on_actionbearbeiten_triggered()
{
    ui->unter_stacked->setCurrentIndex(1);
    ui->GebutsDatum_2->setDate(QDate(2000,01,01));
    m_f_z.set_geburts_datum(QDate(2000,01,01));
}

void Verwaltung::on_actionhinzuf_gen_2_triggered()
{
    ui->unter_stacked->setCurrentIndex(3);
    ui->GebutsDatum->setDate(QDate(2000,01,01));
    m_bearbeiten.set_geburts_datum(QDate(2000,01,01));
}

void Verwaltung::on_actionTable_triggered()
{
    ui->unter_stacked->setCurrentIndex(0);
}


void Verwaltung::on_actionzahlen_triggered()
{
    ui->ober_stacked->show();
}

void Verwaltung::on_actionTabelle_triggered()
{
    ui->unter_stacked->setCurrentIndex(2);
}



void Verwaltung::on_abgeben_button_clicked()
{
    QMessageBox msgBox;
    msgBox.setInformativeText("Moechten Sie wirklich die Kasse leeren ?");
    msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Ok);
    int ret = msgBox.exec();
    switch (ret) {
      case QMessageBox::Ok:
          // Ok was clicked
        {
        QMessageBox msgBox;
        Kasse z;
        double betrag = zahlungManager.get_kasse();
        if (betrag > 0){
            z.set_betrag(betrag);
            z.set_datum(QDate::currentDate());
            int id = zahlungManager.add_kasse(z);
            if(id > 0){
                kasse_update();
                qDebug(logDebug()) << "kasse mit der "<< "ID : "<< id << "wurde abgegeben mit dem Betrag : "<<z.get_betrag();
                msgBox.setText("Kasse wurde erfolgreich entleeren...");
            }else {
                qDebug(logWarning()) << "Fehler bei Kasse entleeren aufgetreten";
                msgBox.setText("Fehler bei Kasse entleeren aufgetreten");
            }
        }else{
            msgBox.setText("Kasse leer...");
        }
        msgBox.exec();
          break;
      }
      case QMessageBox::Cancel:
          // Cancel  was clicked
          break;
      default:
          // should never be reached
          break;
    }
}

void Verwaltung::on_actionKasse_triggered()
{
    ui->unter_stacked->setCurrentIndex(4);
}

void Verwaltung::on_mitglied_table_mehr_clicked()
{
    if(!mitgliedManager.fill_table()){
        //ui->mitglied_table_mehr->hide();
    }
}

void Verwaltung::on_zahlung_table_mehr_clicked()
{
    if(!zahlungManager.fill_zahlung_table()){
        //ui->zahlung_table_mehr->hide();
    }
}

void Verwaltung::on_kasse_table_mehr_clicked()
{
    if(!zahlungManager.fill_kasse_table()){
        //ui->kasse_table_mehr->hide();
    }
}

void Verwaltung::on_suche_bottom_accepted()
{
    mitgliedManager.suche_table_leeren();
    zahlungManager.suche_table_leeren();
    mitgliedManager.get_mitglied(person);
}


void Verwaltung::on_suche_comboBox_currentIndexChanged(const QString &arg1)
{
    person.set_Person_id(arg1.toInt());
}

void Verwaltung::on_suche_vorname_textChanged(const QString &arg1)
{
    person.set_vorname(arg1);
    person.set_Person_id(0);
}

void Verwaltung::on_suche_Nachname_textChanged(const QString &arg1)
{
    person.set_nachname(arg1);
    person.set_Person_id(0);
}

void Verwaltung::on_suche_bottom_rejected()
{
    person.clear();
    ui->suche_vorname->setText("");
    ui->suche_Nachname->setText("");
    ui->unter_stacked->setCurrentIndex(2);
}




void Verwaltung::on_actionsuche_mitglied_triggered()
{
    ui->unter_stacked->setCurrentIndex(5);
    mitgliedManager.suche_table_leeren();
    zahlungManager.suche_table_leeren();
}

void Verwaltung::on_Suche_bottom_clicked()
{
    ui->actionsuche_mitglied->trigger();
}

void Verwaltung::on_area_zu_clicked()
{
    ui->suche_zahlung_area->hide();
}

void Verwaltung::on_actionNoch_nicht_bezahlt_triggered()
{
    ui->actionsuche_mitglied->trigger();
    mitgliedManager.get_noch_nicht_bezahlt();
}
