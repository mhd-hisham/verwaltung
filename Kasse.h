#ifndef KASSE_H
#define KASSE_H

//#include <QDate>
#include "header.h"
class Kasse
{
public:
    Kasse(int kasse_nr);
    Kasse();
    Kasse* set_kasse_nr(int nr);
    Kasse* set_betrag(double betrag);
    Kasse* set_datum(QDate datum);

    int get_kasse_nr();
    double get_betrag();
    QDate get_datum();
    Kasse& operator=(const Kasse& z);

private:
    int kasse_nr;
    double betrag;
    QDate datum;
};

#endif // KASSE_H
