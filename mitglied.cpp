#include "mitglied.h"
Mitglied::Mitglied(){
}
Mitglied::~Mitglied(){}
Mitglied::Mitglied(int mitglied_id){
    this->mitglied_id = mitglied_id;
}
Mitglied* Mitglied::set_vorname(QString name){
    this->vorname = name;
    return  this;
}
Mitglied* Mitglied::set_nachname(QString name){
    this->nachname = name;
    return  this;
}
Mitglied* Mitglied::set_alter(int alter){
    this->alter = alter;
    return this;
}
Mitglied* Mitglied::set_mitglied_id(int nr){
    this->mitglied_id = nr;
    return this;
}
Mitglied* Mitglied::set_geburts_datum(QDate datum){
    this->geburts_datum = datum;
    return this;
}
Mitglied* Mitglied::set_mitglied_datum(QDate datum){
    this->mitglied_datum = datum;
    return this;
}
Mitglied* Mitglied::set_adress(Adress adress){
    this->adress = adress;
    return this;
}
Mitglied* Mitglied::set_betrag(double betrag){
    this->betrag = betrag;
    return this;
}

Mitglied* Mitglied::set_telefon(QString telefon){
    this->telefon = telefon;
    return this;
}

QString Mitglied::get_vorname(){
    return this->vorname;
}
QString Mitglied::get_nachname(){
    return this->nachname;
}
int Mitglied::get_alter(){
    return this->alter;
}
int Mitglied::get_mitglied_id(){
    return this->mitglied_id;
}
QDate Mitglied::get_geburts_datum(){
    return this->geburts_datum;
}
QDate Mitglied::get_mitglied_datum(){
    return this->mitglied_datum;
}
Adress Mitglied::get_adress(){
    return this->adress;
}
double Mitglied::get_betrag(){
    return this->betrag;
}
QString Mitglied::get_telefon(){
    return this->telefon;
}

Adress* Mitglied::set_ort(QString ort){
    this->adress.set_Ort(ort);
    return &this->adress;
}
Adress* Mitglied::set_plz(QString plz){
    this->adress.set_PLZ(plz);
    return &this->adress;
}
Adress* Mitglied::set_strasse(QString str){
    this->adress.set_Strasse(str);
    return &this->adress;
}
Adress* Mitglied::set_haus_nr(QString haus){
    this->adress.set_hausNr(haus);
    return &this->adress;
}

Mitglied& Mitglied::operator=(const Mitglied& m){
    this->set_alter(m.alter);
    this->set_adress(m.adress);
    this->set_betrag(m.betrag);
    this->set_telefon(m.telefon);
    this->set_vorname(m.vorname);
    this->set_nachname(m.nachname);
    this->set_mitglied_id(m.mitglied_id);
    this->set_geburts_datum(m.geburts_datum);
    this->set_mitglied_datum(m.mitglied_datum);
    this->set_last_payment(m.last_payment);
    return *this;
}

void Mitglied::clear(){
    this->set_ort("");
    this->set_plz("");
    this->set_alter(0);
    this->set_betrag(0.0);
    this->set_telefon("");
    this->set_haus_nr("");
    this->set_strasse("");
    this->set_vorname("");
    this->set_nachname("");
    this->set_mitglied_id(0);
}

bool Mitglied::is_valid(){
    return this->get_vorname() != "" &&
            this->get_nachname() != "" &&
            this->get_betrag() != 0.0;

}

Mitglied* Mitglied::set_last_payment(QDate date){
    this->last_payment = date;
    return this;
}
QDate Mitglied::get_last_payment(){
    return this->last_payment;
}
