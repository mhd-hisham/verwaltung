-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 02. Okt 2019 um 15:00
-- Server-Version: 10.4.6-MariaDB
-- PHP-Version: 7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `verwaltung`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `kasse`
--

CREATE TABLE `kasse` (
  `ID` int(11) NOT NULL,
  `datum` date NOT NULL,
  `betrag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `kasse`
--

INSERT INTO `kasse` (`ID`, `datum`, `betrag`) VALUES
(1, '2019-10-02', 554),
(2, '2019-10-02', 88),
(3, '2019-10-02', 389);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `person`
--

CREATE TABLE `person` (
  `ID` int(11) NOT NULL,
  `Vorname` varchar(30) CHARACTER SET ascii NOT NULL,
  `Nachname` varchar(30) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `betrag` double NOT NULL,
  `telefon` varchar(30) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `ort` varchar(30) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `plz` varchar(30) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `strasse` varchar(30) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `haus_nr` varchar(30) CHARACTER SET latin1 COLLATE latin1_danish_ci NOT NULL,
  `mitglied_datum` date NOT NULL,
  `geburts_datum` date NOT NULL,
  `last_payment` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `person`
--

INSERT INTO `person` (`ID`, `Vorname`, `Nachname`, `betrag`, `telefon`, `ort`, `plz`, `strasse`, `haus_nr`, `mitglied_datum`, `geburts_datum`, `last_payment`) VALUES
(1, 'hisham', 'yxc', 44, 'sdf', 'sdf', 'plz1', 'str1', 'haus1', '2019-01-18', '2000-01-01', '2019-10-02'),
(2, 'ert', 'ert', 45, 'ert', 'ert', 'ertert', 'ert', 'ert', '2019-09-14', '2003-01-01', '0000-00-00'),
(3, 'ss', 'ss', 22, 'sss', 'ss', 'ss', 'ss', 'ss', '2019-09-14', '1998-01-01', '0000-00-00'),
(4, 'ddfdfg', 'nh', 66, 'hh', 'thrh', 'trh', 'rth66', 'rth', '2019-09-14', '2001-01-01', '2019-09-14'),
(5, 'asd', 'sd', 33, 'asd', 'd', 'd', 'd', 'asd', '2019-09-15', '2000-01-03', '2019-10-02'),
(6, 'sss', 'aas', 345, 'fsdfsdf', 'dfsdf', 'sdfsdfs', 'sdfsdf', 'sdfsdf', '2019-09-15', '2002-01-01', '2019-10-02'),
(7, 'cvb', 'cvb', 44, 'cvb', 'cvb', 'cvb', 'cvb', 'cvb', '2019-09-16', '2000-01-01', '2019-10-02');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `zahlung`
--

CREATE TABLE `zahlung` (
  `ID` int(11) NOT NULL,
  `mitglied_nr` int(11) NOT NULL,
  `betrag` double NOT NULL,
  `datum` date NOT NULL,
  `Abgegeben` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `zahlung`
--

INSERT INTO `zahlung` (`ID`, `mitglied_nr`, `betrag`, `datum`, `Abgegeben`) VALUES
(24, 1, 44, '2019-10-02', 1),
(25, 1, 44, '2019-10-02', 1),
(26, 1, 44, '2019-10-02', 1),
(27, 1, 44, '2019-10-02', 1),
(28, 5, 33, '2019-10-02', 1),
(29, 6, 345, '2019-10-02', 1),
(30, 7, 44, '2019-10-02', 1),
(31, 7, 44, '2019-10-02', 1),
(32, 1, 44, '2019-10-02', 1),
(33, 6, 345, '2019-10-02', 1);

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `kasse`
--
ALTER TABLE `kasse`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `person`
--
ALTER TABLE `person`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `ID` (`ID`);

--
-- Indizes für die Tabelle `zahlung`
--
ALTER TABLE `zahlung`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `ID` (`ID`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `kasse`
--
ALTER TABLE `kasse`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT für Tabelle `person`
--
ALTER TABLE `person`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT für Tabelle `zahlung`
--
ALTER TABLE `zahlung`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
