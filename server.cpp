#include "server.h"
#include "loggingcategories.h"
#include <QStringList>
Server::Server()
{
}


list<Mitglied> Server::get_Mitglieds(){
    list<Mitglied> mitglied_list;
    qDebug(logDebug()) << "Server get Mitglieds";
    QSqlDatabase db = QSqlDatabase::database();
    if(db.open()){
        qDebug(logDebug())<< "Database connected";
        QString str = "select * from person limit "+QString::number(mitglied_current_index)+",5";
        QSqlQuery qry;
        qry.prepare(str);
        if(!qry.exec()){
            qDebug (logDebug()) << qry.lastError().text();
        }else{
            while(qry.next()){
                Mitglied m(qry.value(0).toInt());
                m.set_vorname(qry.value(1).toString())
                        ->set_nachname(qry.value(2).toString())
                        ->set_betrag(qry.value(3).toDouble())
                        ->set_geburts_datum(QDate::fromString(qry.value(10).toString(),"yyyy-MM-dd"))
                        ->set_mitglied_datum(QDate::fromString(qry.value(9).toString(),"yyyy-MM-dd"))
                        ->set_last_payment(QDate::fromString(qry.value(11).toString(),"yyyy-MM-dd"))
                        ->set_telefon(qry.value(4).toString())
                        ->set_ort(qry.value(5).toString())
                        ->set_PLZ(qry.value(6).toString())
                        ->set_hausNr(qry.value(8).toString())
                        ->set_Strasse(qry.value(7).toString());
                mitglied_list.push_back(m);
             }
            mitglied_current_index = mitglied_current_index + 5;
        }
    }else{
        qDebug(logDebug()) << db.lastError().text();
    }
    db.close();
    return mitglied_list;
}

list<QPair<int,QString>> Server::get_Mitglied_kurz(){
    list<QPair<int,QString>> mitglied_list;
    qDebug(logDebug()) << "Server get Mitglied kurz";
    QSqlDatabase db = QSqlDatabase::database();
    if(db.open()){
        qDebug(logDebug())<< "Database connected";
        QString str = "select ID , Nachname from person";
        QSqlQuery qry;
        qry.prepare(str);
        if(!qry.exec()){
            qDebug (logDebug()) << qry.lastError().text();
        }else{
            while(qry.next()){
                auto m = qMakePair(qry.value(0).toInt(),qry.value(1).toString());
                mitglied_list.push_back(m);
             }
        }
    }else{
        qDebug(logDebug()) << db.lastError().text();
    }
    db.close();
    return mitglied_list;
}


list<Zahlung> Server::get_Zahlungen(){
    qDebug(logDebug()) << "Server get Zahlungen";
    list<Zahlung> zahlung_list;
    QSqlDatabase db = QSqlDatabase::database();
    if(db.open()){
        qDebug(logDebug())<< "Database connected";
        QString str = "select * from zahlung limit "+QString::number(zahlung_current_index)+",10";
        QSqlQuery qry;
        qry.prepare(str);
        if(!qry.exec()){
            qDebug (logDebug()) << qry.lastError().text();
        }else{
            while(qry.next()){
                Zahlung z(qry.value(0).toInt());
                z.set_datum(QDate::fromString(qry.value(3).toString(),"yyyy-MM-dd"))
                        ->set_betrag(qry.value(2).toDouble())
                        ->set_mitglied_nr(qry.value(1).toInt())
                        ->abgegeben(qry.value(4).toBool());
                zahlung_list.push_back(z);
             }
            zahlung_current_index = zahlung_current_index + 10;
        }
    }else{
        qDebug(logDebug()) << db.lastError().text();
    }

    db.close();
    return zahlung_list;
}

list<Kasse> Server::get_Kassen(){
    qDebug(logDebug()) << "Server get Kassen";
    list<Kasse> kasse_list;
    QSqlDatabase db = QSqlDatabase::database();
    if(db.open()){
        qDebug(logDebug())<< "Database connected";
        QString str = "select * from kasse limit "+QString::number(kasse_current_index)+",10";
        QSqlQuery qry;
        qry.prepare(str);
        if(!qry.exec()){
            qDebug (logDebug()) << qry.lastError().text();
        }else{
            while(qry.next()){
                Kasse z(qry.value(0).toInt());
                z.set_datum(QDate::fromString(qry.value(1).toString(),"yyyy-MM-dd"))
                        ->set_betrag(qry.value(2).toDouble());
                kasse_list.push_back(z);
             }
            kasse_current_index = kasse_current_index + 10;
        }
    }else{
        qDebug(logDebug()) << db.lastError().text();
    }
    db.close();
    return kasse_list;
}


double Server::get_kasse(){
    qDebug(logDebug()) << "Server get die aktuelle kasse";
    double kasse = 0.0;
    QSqlDatabase db = QSqlDatabase::database();
    if(db.open()){
        qDebug(logDebug())<< "Database connected";
        QString str = "select sum(betrag) from zahlung where Abgegeben = 0";
        QSqlQuery qry;
        qry.prepare(str);
        if(!qry.exec()){
            qDebug (logDebug()) << qry.lastError().text();
        }else{
            qry.next();
            kasse = qry.value(0).toDouble();
        }
    }else{
        qDebug(logDebug()) << db.lastError().text();
    }

    db.close();
    return kasse;
}


int Server::set_mitglied(Mitglied m){
    int res = -1;
    QSqlDatabase db = QSqlDatabase::database();
    if(db.open()){
        qDebug()<< "connected";

        QString str = "insert into person(Vorname, Nachname, betrag, telefon, ort, "
                                         "plz, strasse, haus_nr, mitglied_datum, "
                                         "geburts_datum) "
                      "values(:Vorname, :Nachname, :betrag, :telefon, :ort, "
                             ":plz, :strasse, :haus_nr, :mitglied_datum, "
                             ":geburts_datum)";
        QSqlQuery qry;

        qry.prepare(str);

        qry.bindValue(":Vorname",m.get_vorname());
        qry.bindValue(":Nachname",m.get_nachname());
        qry.bindValue(":betrag",m.get_betrag());
        qry.bindValue(":telefon",m.get_telefon());
        qry.bindValue(":ort",m.get_adress().get_ort());
        qry.bindValue(":plz",m.get_adress().get_PLZ());
        qry.bindValue(":strasse",m.get_adress().get_strasse());
        qry.bindValue(":haus_nr",m.get_adress().get_hausNr());
        qry.bindValue(":mitglied_datum",QDate::currentDate().toString("yyyy-MM-dd"));
        qry.bindValue(":geburts_datum",m.get_geburts_datum().toString("yyyy-MM-dd"));

        if(!qry.exec()){
            qDebug () << qry.lastError().text();
            res = -1;
        }else{
            res = qry.lastInsertId().toInt();
        }

    }else{
        qDebug () << db.lastError().text();
        res = -1;
    }
    db.close();
    return res;
}


Mitglied Server::get_mitglied(int id){
    Person p(id);
    list<Mitglied> res = get_mitglied(p);
    if(!res.empty()){
        return res.back();
    }
    Mitglied m;
    return m;
}

list<Mitglied> Server::get_mitglied(Person p){
    qDebug(logDebug()) << "Server suche Mitglied";
    list<Mitglied> res;
    QStringList where ;
    if(p.get_Person_id() > 0){
        where.append(" ID = "+QString::number(p.get_Person_id()));
    }
    if(p.get_vorname() != ""){
        where.append(" Vorname = \""+p.get_vorname()+"\"");
    }
    if(p.get_nachname() != ""){
        where.append( " Nachname = \""+p.get_nachname()+"\"");
    }

    qDebug(logDebug())<< where.join(" and ");
    QSqlDatabase db = QSqlDatabase::database();
    if(db.open()){
        qDebug(logDebug())<< "Database connected";
        QString str = "select * from person where "+where.join(" and ");
        QSqlQuery qry;
        qry.prepare(str);
        if(!qry.exec()){
            qDebug (logDebug()) << qry.lastError().text();
        }else{
            while(qry.next()){
                Mitglied m(qry.value(0).toInt());
                m.set_vorname(qry.value(1).toString())
                        ->set_nachname(qry.value(2).toString())
                        ->set_betrag(qry.value(3).toDouble())
                        ->set_geburts_datum(QDate::fromString(qry.value(10).toString(),"yyyy-MM-dd"))
                        ->set_mitglied_datum(QDate::fromString(qry.value(9).toString(),"yyyy-MM-dd"))
                        ->set_last_payment(QDate::fromString(qry.value(11).toString(),"yyyy-MM-dd"))
                        ->set_telefon(qry.value(4).toString())
                        ->set_ort(qry.value(5).toString())
                        ->set_PLZ(qry.value(6).toString())
                        ->set_hausNr(qry.value(8).toString())
                        ->set_Strasse(qry.value(7).toString());
                res.push_back(m);
             }
        }
    }else{
        qDebug(logDebug()) << db.lastError().text();
    }
    db.close();
    return res;
}

list<Mitglied> Server::get_noch_nicht_bezahlt(){
    qDebug(logDebug()) << "Server get_noch_nicht_bezahlt";
    list<Mitglied> res;
    QSqlDatabase db = QSqlDatabase::database();
    if(db.open()){
        qDebug(logDebug())<< "Database connected";
        QString str = "SELECT * from person WHERE (CURRENT_DATE - last_payment) > 30 ";
        qDebug(logDebug())<< str;
        QSqlQuery qry;
        qry.prepare(str);
        if(!qry.exec()){
            qDebug (logDebug()) << qry.lastError().text();
        }else{
            while(qry.next()){
                Mitglied m(qry.value(0).toInt());
                m.set_vorname(qry.value(1).toString())
                        ->set_nachname(qry.value(2).toString())
                        ->set_betrag(qry.value(3).toDouble())
                        ->set_geburts_datum(QDate::fromString(qry.value(10).toString(),"yyyy-MM-dd"))
                        ->set_mitglied_datum(QDate::fromString(qry.value(9).toString(),"yyyy-MM-dd"))
                        ->set_last_payment(QDate::fromString(qry.value(11).toString(),"yyyy-MM-dd"))
                        ->set_telefon(qry.value(4).toString())
                        ->set_ort(qry.value(5).toString())
                        ->set_PLZ(qry.value(6).toString())
                        ->set_hausNr(qry.value(8).toString())
                        ->set_Strasse(qry.value(7).toString());
                res.push_back(m);
             }
        }
    }else{
        qDebug(logDebug()) << db.lastError().text();
    }
    db.close();
    return res;
}

int Server::update_mitglied(Mitglied m){
    int res = -1;
    QSqlDatabase db = QSqlDatabase::database();
    if(db.open()){
        qDebug()<< "connected";

        QString str = "UPDATE person SET Vorname = :Vorname,"
                                " Nachname = :Nachname, betrag = :betrag,"
                                " telefon = :telefon, ort = :ort, plz = :plz,"
                                " strasse = :strasse, haus_nr = :haus_nr,"
                                " geburts_datum = :geburts_datum"
                      " WHERE person.ID = :ID;";
        QSqlQuery qry;

        qry.prepare(str);

        qry.bindValue(":ID",m.get_mitglied_id());
        qry.bindValue(":Vorname",m.get_vorname());
        qry.bindValue(":Nachname",m.get_nachname());
        qry.bindValue(":betrag",m.get_betrag());
        qry.bindValue(":telefon",m.get_telefon());
        qry.bindValue(":ort",m.get_adress().get_ort());
        qry.bindValue(":plz",m.get_adress().get_PLZ());
        qry.bindValue(":strasse",m.get_adress().get_strasse());
        qry.bindValue(":haus_nr",m.get_adress().get_hausNr());
        qry.bindValue(":geburts_datum",m.get_geburts_datum().toString("yyyy-MM-dd"));

        if(!qry.exec()){
            qDebug () << qry.lastError().text();
        }else{
            qDebug () <<"Mitglied : "<< m.get_mitglied_id()<< "wird bearbeitet";
            res = 1;
        }

    }else{
        qDebug () << db.lastError().text();
    }
    db.close();

    return res;
}



int Server::set_zahlung(Zahlung z){
    int res = -1;
    QSqlDatabase db = QSqlDatabase::database();
    if(db.open()){
        qDebug()<< "connected";

        QString str = "insert into zahlung(mitglied_nr, betrag, datum) "
                      "values(:mitglied_nr, :betrag, :datum)";
        QSqlQuery qry;

        qry.prepare(str);

        qry.bindValue(":mitglied_nr",z.get_mitglied_nr());
        qry.bindValue(":betrag",z.get_betrag());
        qry.bindValue(":datum",z.get_datum().toString("yyyy-MM-dd"));

        if(!qry.exec()){
            qDebug () << qry.lastError().text();
            res = -1;
            db.close();
            return res;
        }else{
            res = qry.lastInsertId().toInt();
        }


        qDebug()<< "Update last payment";

        str = "UPDATE person SET last_payment = :last_payment"
                      " WHERE person.ID = :ID;";

        qry.prepare(str);

        qry.bindValue(":last_payment",z.get_datum().toString("yyyy-MM-dd"));
        qry.bindValue(":ID",z.get_mitglied_nr());

        if(!qry.exec()){
            qDebug()<< "last payment fehler";
            qDebug () << qry.lastError().text();
            qDebug() << QSqlDatabase::database().rollback();
        }
    }else{
        qDebug () << db.lastError().text();
        res = -1;
    }
    db.close();
    return res;
}

list<Zahlung> Server::get_zahlung(int id){
    qDebug(logDebug()) << "Server get Zahlungen nach mitglied ID";
    list<Zahlung> zahlung_list;
    QSqlDatabase db = QSqlDatabase::database();
    if(db.open()){
        qDebug(logDebug())<< "Database connected";
        QString str = "select * from zahlung where mitglied_nr = "+QString::number(id);
        QSqlQuery qry;
        qry.prepare(str);
        if(!qry.exec()){
            qDebug (logDebug()) << qry.lastError().text();
        }else{
            while(qry.next()){
                Zahlung z(qry.value(0).toInt());
                z.set_datum(QDate::fromString(qry.value(3).toString(),"yyyy-MM-dd"))
                        ->set_betrag(qry.value(2).toDouble())
                        ->set_mitglied_nr(qry.value(1).toInt())
                        ->abgegeben(qry.value(4).toBool());
                zahlung_list.push_back(z);
             }
        }
    }else{
        qDebug(logDebug()) << db.lastError().text();
    }

    db.close();
    return zahlung_list;

}



int Server::set_kasse(Kasse k){
    int res = -1;
    QSqlDatabase db = QSqlDatabase::database();
    if(db.open()){
        qDebug()<< "connected";

        QString str = "insert into kasse(betrag, datum) "
                      "values(:betrag, :datum)";
        QSqlQuery qry;

        qry.prepare(str);

        qry.bindValue(":betrag",k.get_betrag());
        qry.bindValue(":datum",k.get_datum().toString("yyyy-MM-dd"));

        if(!qry.exec()){
            qDebug () << qry.lastError().text();
            res = -1;
        }else{
            res = qry.lastInsertId().toInt();

            qDebug()<< "Update Zahlung abgeben";

            str = "UPDATE zahlung SET Abgegeben = true"
                          " WHERE zahlung.Abgegeben = 0;";

            qry.prepare(str);

            if(!qry.exec()){
                qDebug() << "Update Zahlung abgeben";
                qDebug () << qry.lastError().text();
                qDebug() << QSqlDatabase::database().rollback();
            }
        }
    }else{
        qDebug () << db.lastError().text();
        res = -1;
    }
    db.close();
    return res;
}


void Server::test(){
    /*manager->connect(manager,&QNetworkAccessManager::finished,
            manager,
              [=](QNetworkReply *reply) {
               if (reply->error()) {
                   return;
               }
               QString answer = reply->readAll();
           }
       );
    request.setUrl(QUrl("http://localhost:3000"));
        manager->get(request);
*/
}





