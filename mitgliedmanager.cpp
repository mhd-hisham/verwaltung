#include "mitgliedmanager.h"



MitgliedManager::~MitgliedManager(){
}

void MitgliedManager::init(){

}

void MitgliedManager::Table_Init(){
    model->setHeaderData(0,Qt::Horizontal,QObject::tr("ID"));
    model->setHeaderData(1,Qt::Horizontal,QObject::tr("Vorname"));
    model->setHeaderData(2,Qt::Horizontal,QObject::tr("Nachname"));
    model->setHeaderData(3,Qt::Horizontal,QObject::tr("Betrag"));
    model->setHeaderData(4,Qt::Horizontal,QObject::tr("Letzte Zahlung"));
    mitglied_table->setModel(model);
    mitglied_table->setSortingEnabled(true);
    mitglied_table->sortByColumn(0, Qt::AscendingOrder);
}

void MitgliedManager::suche_Table_Init(){
    suche_model->setHeaderData(0,Qt::Horizontal,QObject::tr("ID"));
    suche_model->setHeaderData(1,Qt::Horizontal,QObject::tr("Vorname"));
    suche_model->setHeaderData(2,Qt::Horizontal,QObject::tr("Nachname"));
    suche_model->setHeaderData(3,Qt::Horizontal,QObject::tr("Betrag"));
    suche_model->setHeaderData(4,Qt::Horizontal,QObject::tr("Letzte Zahlung"));
    suchen_table->setModel(suche_model);
    suchen_table->setSortingEnabled(true);
    suchen_table->sortByColumn(0, Qt::AscendingOrder);
}

MitgliedManager::MitgliedManager(){
}

bool MitgliedManager::fill_table(){
    list<Mitglied> list = server->get_Mitglieds();
    if(list.empty()){
        return false;
    }
    for_each(list.begin(),list.end(),[&](Mitglied m){
       add_mitglied(m,true);
    });
    return true;
}

bool MitgliedManager::fill_comboboxs(){

    list<QPair<int,QString>> list = server->get_Mitglied_kurz();
    if(list.empty()){
        return false;
    }

    //Erts alles leeren vielleicht wird beim Update aufgerufen
    for_each(nachname_combo_list.begin(),nachname_combo_list.end(),[&](std::shared_ptr<QComboBox> &com){
        com->clear();
    });
    for_each(Id_combo_list.begin(),Id_combo_list.end(),[&](std::shared_ptr<QComboBox> &com){
        com->clear();
    });

    for_each(list.begin(),list.end(),[&](QPair<int,QString> it){
        for_each(nachname_combo_list.begin(),nachname_combo_list.end(),[&](std::shared_ptr<QComboBox> &com){
            com->addItem(it.second);
        });
        for_each(Id_combo_list.begin(),Id_combo_list.end(),[&](std::shared_ptr<QComboBox> &com){
            com->addItem((QString::number(it.first)));
        });
    });
    return true;
}

bool MitgliedManager::add_mitglied(Mitglied m,bool from_server){
    int id = 0;
    if(!from_server){
            id = server->set_mitglied(m);
            if(id < 1){
                return false;
            }
            m.set_mitglied_id(id);

            for_each(nachname_combo_list.begin(),nachname_combo_list.end(),[&](std::shared_ptr<QComboBox> &com){
                com->addItem(m.get_nachname());
            });
            for_each(Id_combo_list.begin(),Id_combo_list.end(),[&](std::shared_ptr<QComboBox> &com){
                com->addItem((QString::number(m.get_mitglied_id())));
            });
    }

    int row = mitglied_table->model()->rowCount();
    model->setItem(row, 0, new QStandardItem(QString::number(m.get_mitglied_id())));
    model->setItem(row, 1, new QStandardItem(m.get_vorname()));
    model->setItem(row, 2, new QStandardItem(m.get_nachname()));
    model->setItem(row, 3, new QStandardItem(QString::number(m.get_betrag())));
    model->setItem(row, 4, new QStandardItem(m.get_last_payment().toString("yyyy-MM-dd")));
    return true;
}

int MitgliedManager::update_mitglied(Mitglied m){
    qDebug() << "update mitglied\n";
    int res = this->update_server(m);
    if(res != -1){
        this->update_list(m);
        this->update_table(m);
        this->update_comboBox();
        return res;
    }else {
        return res;
    }
}

void MitgliedManager::update_list(Mitglied m){
    qDebug() << "update list\n";
    for_each(mitglied_list.begin(),mitglied_list.end(),[&m](Mitglied &mitglied){
        if(mitglied.get_mitglied_id() == m.get_mitglied_id()){
            mitglied = m;
        }
    });
}
void MitgliedManager::update_table(Mitglied m){
    qDebug() << "update table\n";
        for(int i = 0; i < mitglied_table->model()->rowCount(); i++){
            if(mitglied_table->model()->data(mitglied_table->model()->index(i,0)).toInt() == m.get_mitglied_id()){
                mitglied_table->model()->setData(mitglied_table->model()->index(i,1),m.get_vorname());
                mitglied_table->model()->setData(mitglied_table->model()->index(i,2),m.get_nachname());
                mitglied_table->model()->setData(mitglied_table->model()->index(i,3),QString::number(m.get_betrag()));
                mitglied_table->model()->setData(mitglied_table->model()->index(i,4),m.get_last_payment().toString("yyyy-MM-dd"));
                mitglied_table->model()->submit();
            }
        }
}
int MitgliedManager::update_server(Mitglied m){
    qDebug() << "update server\n";
    return server->update_mitglied(m);
}

void MitgliedManager::update_comboBox(){
    qDebug() << "update comboBox\n";
    this->fill_comboboxs();
}


Mitglied MitgliedManager::get_mitglied(int mitglied_nr){
    Mitglied res;
    for_each(mitglied_list.begin(),mitglied_list.end(),[&](Mitglied m){
        if(m.get_mitglied_id() == mitglied_nr){
            res = m;
        }
    });
    if(res.is_valid()){
        return res;
    }else{
        res = server->get_mitglied(mitglied_nr);
        if(res.is_valid()){
            if(mitglied_list.size() == 10){
                mitglied_list.pop_back();
            }
            mitglied_list.push_front(res);
        }
    }
    return res;
}

void MitgliedManager::get_mitglied(Person p){
    suche_list = server->get_mitglied(p);
    for_each(suche_list.begin(),suche_list.end(),[&](Mitglied m){
        int row = suchen_table->model()->rowCount();
        suche_model->setItem(row, 0, new QStandardItem(QString::number(m.get_mitglied_id())));
        suche_model->setItem(row, 1, new QStandardItem(m.get_vorname()));
        suche_model->setItem(row, 2, new QStandardItem(m.get_nachname()));
        suche_model->setItem(row, 3, new QStandardItem(QString::number(m.get_betrag())));
        suche_model->setItem(row, 4, new QStandardItem(m.get_last_payment().toString("yyyy-MM-dd")));
    });
}

void MitgliedManager::get_noch_nicht_bezahlt(){
    suche_list = server->get_noch_nicht_bezahlt();
    for_each(suche_list.begin(),suche_list.end(),[&](Mitglied m){
        int row = suchen_table->model()->rowCount();
        suche_model->setItem(row, 0, new QStandardItem(QString::number(m.get_mitglied_id())));
        suche_model->setItem(row, 1, new QStandardItem(m.get_vorname()));
        suche_model->setItem(row, 2, new QStandardItem(m.get_nachname()));
        suche_model->setItem(row, 3, new QStandardItem(QString::number(m.get_betrag())));
        suche_model->setItem(row, 4, new QStandardItem(m.get_last_payment().toString("yyyy-MM-dd")));
    });
}


void MitgliedManager::set_comboBox(QComboBox *com, ComboBox_Typ type){
    if(type == ComboBox_Typ::Nachname)
    {
        if(!this->nachname_combo_list.empty()){
            int count = this->nachname_combo_list.front()->count();
            for (int i = 0 ; i < count ; i++){
                com->addItem(this->nachname_combo_list.front()->itemText(i));
            }
        }
        this->nachname_combo_list.push_back(std::shared_ptr<QComboBox>(com));
    }else if(type == ComboBox_Typ::ID){
        if(!this->Id_combo_list.empty()){
            int count = this->Id_combo_list.front()->count();
            for (int i = 0 ; i < count ; i++){
                com->addItem(this->Id_combo_list.front()->itemText(i));
            }
        }
        this->Id_combo_list.push_back(std::shared_ptr<QComboBox>(com));
    }

}

void MitgliedManager::set_Table(QTableView *table){
    mitglied_table.reset(table);
    Table_Init();
}

void MitgliedManager::set_suche_Table(QTableView *table){
    suchen_table.reset(table);
    suche_Table_Init();
}


void MitgliedManager::print_table(){
    QString strStream;
    QTextStream out(&strStream);

    const int rowCount = mitglied_table->model()->rowCount();
    const int columnCount = mitglied_table->model()->columnCount();

    out <<  "<html>\n"
        "<head>\n"
        "<meta Content=\"Text/html; charset=Windows-1251\">\n"
        <<  QString("<title>%1</title>\n").arg("strTitle")
        <<  "</head>\n"
        "<body bgcolor=#ffffff link=#5000A0>\n"
        "<table border=1 cellspacing=0 cellpadding=2>\n";

    // headers
    out << "<thead><tr bgcolor=#f0f0f0>";
    for (int column = 0; column < columnCount; column++)
        if (!mitglied_table->isColumnHidden(column))
            out << QString("<th>%1</th>").arg(mitglied_table->model()->headerData(column, Qt::Horizontal).toString());
    out << "</tr></thead>\n";

    // data table
    for (int row = 0; row < rowCount; row++) {
        out << "<tr>";
        for (int column = 0; column < columnCount; column++) {
            if (!mitglied_table->isColumnHidden(column)) {
                QString data = mitglied_table->model()->data(mitglied_table->model()->index(row, column)).toString().simplified();
                out << QString("<td bkcolor=0>%1</td>").arg((!data.isEmpty()) ? data : QString("&nbsp;"));
            }
        }
        out << "</tr>\n";
    }
    out <<  "</table>\n"
        "</body>\n"
        "</html>\n";

    QTextDocument *document = new QTextDocument();
    document->setHtml(strStream);

    QPrinter printer;

    QPrintDialog *dialog = new QPrintDialog(&printer, NULL);
    if (dialog->exec() == QDialog::Accepted) {
        document->print(&printer);
    }

    delete document;
}


void MitgliedManager::suche_table_leeren(){
    while (suchen_table->model()->rowCount() > 0) {
        suchen_table->model()->removeRow(0);
    }
}




