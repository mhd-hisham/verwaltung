#include "calender.h"

Calender::Calender()
{

}


Calender& Calender::instance(){
    static Calender INSTANCE;
    return  INSTANCE;
}
void Calender::set_calender(QCalendarWidget * cal, QListWidget * list){
    this->myCalendar.reset(cal);
    this->mylist.reset(list);
    for(auto it : hash.keys()){
       for(auto it2 : hash.take(it))
           mylist->addItem(it2);
       QTextCharFormat format;
       format.setFontUnderline(true);
       format.setFontItalic(true);
       format.setForeground(Qt::green);
       myCalendar->setDateTextFormat(it,format);
    }
}

void Calender::select_date(QDate date,Qt::GlobalColor color, QString info){
    QTextCharFormat format;
    format.setFontUnderline(true);
    format.setFontItalic(true);
    format.setForeground(color);
    myCalendar->setDateTextFormat(date,format);
    hash[date].push_front(info);
    mylist->addItem(info);
}


