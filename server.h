#ifndef SERVER_H
#define SERVER_H

#include "mitglied.h"
#include "zahlung.h"
#include "kasse.h"
#include "header.h"
#include "person.h"

using namespace std;
class Server
{
public:

    ~Server(){
        //delete manager;
    }
    int set_mitglied(Mitglied m);
    Mitglied get_mitglied(int id);
    list<Mitglied> get_mitglied(Person p);
    list<Mitglied> get_noch_nicht_bezahlt();
    int update_mitglied(Mitglied m);
    static Server& getInstance(){
        static Server server;
        return server;
    }
    int set_zahlung(Zahlung z);
    int set_kasse(Kasse k);
    list<Zahlung> get_zahlung(int id);

    list<Mitglied> get_Mitglieds();
    list<QPair<int,QString>> get_Mitglied_kurz();
    list<Zahlung> get_Zahlungen();
    list<Kasse> get_Kassen();
    double get_kasse();
    void test();


private:
    Server();

    bool wait = true;
    int mitglied_current_index = 0;
    int zahlung_current_index = 0;
    int kasse_current_index = 0;

};

#endif // SERVER_H
