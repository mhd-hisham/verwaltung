#ifndef PERSON_H
#define PERSON_H
#include "header.h"
using namespace std;

class Person
{
public:
    Person();
    Person(int Person_id);
    Person* set_vorname(QString name);
    Person* set_nachname(QString name);
    Person* set_Person_id(int nr);

    QString get_vorname();
    QString get_nachname();
    int get_Person_id();

    void clear();
    ~Person();
    Person& operator=(const Person& m);
    bool is_valid();
private:
    QString vorname;
    QString nachname;
    int Person_id;
};

#endif // PERSON_H
