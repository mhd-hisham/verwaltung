#ifndef ZAHLUNGMANAGER_H
#define ZAHLUNGMANAGER_H

#include "zahlung.h"
#include "Kasse.h"
#include "server.h"
#include "header.h"


class ZahlungManager
{
public:
    ZahlungManager();
    ~ZahlungManager(){
    }
    bool add_zahlung(Zahlung zahlung,bool from_server=false);
    bool add_kasse(Kasse kasse,bool from_server=false);
    void get_Zahlung(int mitglied_nr);

    void set_Table(QTableView *zahlung_table, QTableView *kasse_table);
    double get_kasse();
    void print_table();
    void set_suche_Table(QTableView *table);

    bool fill_zahlung_table();
    bool fill_kasse_table();
    void suche_table_leeren();
    list<Zahlung> suche_list;
private:
    void init();
    void Table_Init();
    void suche_Table_Init();
    void update_table(Zahlung m);

    std::unique_ptr<QTableView> zahlung_table = std::unique_ptr<QTableView>(new QTableView);
    std::unique_ptr<QTableView> kasse_table = std::unique_ptr<QTableView>(new QTableView);
    std::unique_ptr<QTableView> suchen_table = std::unique_ptr<QTableView>(new QTableView);
    QStandardItemModel *model = new QStandardItemModel(0,4);
    QStandardItemModel *suche_model = new QStandardItemModel(0,4);

    QStandardItemModel *kasse_model = new QStandardItemModel(0,3);
    std::shared_ptr<Server> server = std::shared_ptr<Server>(&Server::getInstance());
};

#endif // ZAHLUNGMANAGER_H
