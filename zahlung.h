#ifndef ZAHLUNG_H
#define ZAHLUNG_H

//#include <QDate>
#include "header.h"

using namespace std;

class Zahlung
{
public:
    Zahlung(int zahlung_nr);
    Zahlung();
    Zahlung* set_zahlung_nr(int nr);
    Zahlung* set_mitglied_nr(int nr);
    Zahlung* set_betrag(double betrag);
    Zahlung* set_datum(QDate datum);
    Zahlung* abgegeben(bool abge);

    int get_zahlung_nr();
    int get_mitglied_nr();
    double get_betrag();
    QDate get_datum();
    bool get_abge();
    Zahlung& operator=(const Zahlung& z);

private:
    int zahlung_nr;
    int mitglied_nr;
    double betrag;
    QDate datum;
    bool abge;
};

#endif // ZAHLUNG_H
