#include "zahlung.h"

Zahlung::Zahlung(){
}
Zahlung::Zahlung(int zahlung_nr){
    this->zahlung_nr=zahlung_nr;
}
Zahlung* Zahlung::set_zahlung_nr(int nr){
    this->zahlung_nr = nr;
    return this;
}
Zahlung* Zahlung::set_mitglied_nr(int nr){
    this->mitglied_nr = nr;
    return this;
}
Zahlung* Zahlung::set_betrag(double betrag){
    this->betrag = betrag;
    return this;
}
Zahlung* Zahlung::set_datum(QDate datum){
    this->datum = datum;
    return this;
}

int Zahlung::get_zahlung_nr(){
    return this->zahlung_nr;
}
int Zahlung::get_mitglied_nr(){
    return this->mitglied_nr;
}
double Zahlung::get_betrag(){
    return this->betrag;
}
QDate Zahlung::get_datum(){
    return this->datum;
}

bool Zahlung::get_abge(){
    return this->abge;
}

Zahlung* Zahlung::abgegeben(bool abge){
    this->abge = abge;
    return this;
}

Zahlung& Zahlung::operator=(const Zahlung &z){
    this->set_betrag(z.betrag);
    this->set_datum(z.datum);
    this->set_zahlung_nr(z.zahlung_nr);
    this->set_mitglied_nr(z.mitglied_nr);
    return *this;
}
