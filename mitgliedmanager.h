#ifndef MITGLEIDMANAGER_H
#define MITGLEIDMANAGER_H


#include "header.h"
#include "server.h"
#include "mitglied.h"
#include "person.h"

class MitgliedManager
{
public:
    MitgliedManager();
    ~MitgliedManager();
    enum ComboBox_Typ{ID,Nachname};
    bool add_mitglied(Mitglied m,bool from_server=false);
    int update_mitglied(Mitglied m);
    Mitglied get_mitglied(int mitglied_nr);
    void get_noch_nicht_bezahlt();
    void set_comboBox(QComboBox *com, ComboBox_Typ type);
    void set_Table(QTableView *table);
    void set_suche_Table(QTableView *table);
    void print_table();
    void get_mitglied(Person p);
    bool fill_table();
    bool fill_comboboxs();
    void suche_table_leeren();
    void init();
    void Table_Init();
    void suche_Table_Init();
    void update_list(Mitglied m);
    void update_table(Mitglied m);
    int  update_server(Mitglied m);
    void update_comboBox();
    list<Mitglied> suche_list;

private:
    QStandardItemModel *model = new QStandardItemModel(0,5);
    QStandardItemModel *suche_model = new QStandardItemModel(0,5);
    std::unique_ptr<QTableView> mitglied_table = std::unique_ptr<QTableView>(new QTableView);
    std::unique_ptr<QTableView> suchen_table = std::unique_ptr<QTableView>(new QTableView);
    list<std::shared_ptr<QComboBox>> nachname_combo_list ;
    list<std::shared_ptr<QComboBox>> Id_combo_list;
    std::shared_ptr<Server> server = std::shared_ptr<Server>(&Server::getInstance());
    list<Mitglied> mitglied_list;

};

#endif // MITGLEIDMANAGER_H
