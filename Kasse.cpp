#include "kasse.h"

Kasse::Kasse()
{

}

Kasse::Kasse(int kasse_nr){
    this->kasse_nr = kasse_nr;
}

Kasse* Kasse::set_kasse_nr(int nr){
    this->kasse_nr = nr;
    return this;
}
Kasse* Kasse::set_betrag(double betrag){
    this->betrag = betrag;
    return this;
}
Kasse* Kasse::set_datum(QDate datum){
    this->datum = datum;
    return this;
}

int Kasse::get_kasse_nr(){
    return this->kasse_nr;
}
double Kasse::get_betrag(){
    return this->betrag;
}
QDate Kasse::get_datum(){
    return this->datum;
}
Kasse& Kasse::operator=(const Kasse& z){
    this->set_datum(z.datum);
    this->set_betrag(z.betrag);
    this->set_kasse_nr(z.kasse_nr);
    return *this;
}
