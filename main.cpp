#include "verwaltung.h"
#include <QApplication>
#include <list>
#include <iostream>
#include "mitglied.h"

#include <QFile>
#include <QDir>
#include <QScopedPointer>
#include <QTextStream>
#include <QDateTime>
#include <QLoggingCategory>
#include <QSql>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QMessageBox>

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>

using namespace std;

QScopedPointer<QFile> m_logFile;

void messageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg);

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    m_logFile.reset(new QFile("logFile.txt"));
    // Open the file logging
    m_logFile.data()->open(QFile::Append | QFile::Text);
    // Set handler
    qInstallMessageHandler(messageHandler);

    QMessageBox msgBox;
    QString hostname,Username,Password,Databasename;
    QFile file("conf.json");
    if(!file.open(QIODevice::ReadOnly | QIODevice::Text)){
        qDebug(logCritical)<<"conf datei kann nicht gelesen werden";
        file.close();
    }else{
        QTextStream in(&file);
        QString str = in.readAll();
        file.close();
        QJsonDocument d = QJsonDocument::fromJson(str.toUtf8());
        qDebug() << d;
        QJsonObject obj = d.object();
        QJsonValue value = obj.value(QString("server"));
        QJsonObject item = value.toObject();
        hostname = item["hostname"].toString();
        Username = item["username"].toString();
        Password = item["password"].toString();
        Databasename = item["Databasename"].toString();
    }

    QSqlDatabase db = QSqlDatabase::addDatabase("QMYSQL");
    db.setHostName(hostname);
    db.setUserName(Username);
    db.setPassword(Password);
    db.setDatabaseName(Databasename);
    if(!db.open()){
        msgBox.setText(db.lastError().text());
        msgBox.exec();
        return -1;
    }


    Verwaltung w;
    w.show();

    return a.exec();
}

void messageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{

    QTextStream out(m_logFile.data());

    out << QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss.zzz ");

    switch (type)
    {
    case QtInfoMsg:     out << "INF "; break;
    case QtDebugMsg:    out << "DBG "; break;
    case QtWarningMsg:  out << "WRN "; break;
    case QtCriticalMsg: out << "CRT "; break;
    case QtFatalMsg:    out << "FTL "; break;
    }

    out << context.category << ": "
        << msg << endl;
    out.flush();
}
